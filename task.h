#ifndef TASK_H
#define TASK_H

#include <unistd.h>
#include <string.h>

#include "arg.h"

#define ARGVSIZE 64

typedef struct {
    int infd, outfd;
    int argc;
    arg_t heap[ARGVSIZE + 1];
    char *argv[ARGVSIZE + 1];
} task_t;

void task_init(task_t *);
void task_setinfd(task_t *, int);
void task_setoutfd(task_t *, int);
int task_argvpush(task_t *, arg_t *);
void task_argvclear(task_t *);

#endif // TASK_H
