#ifndef ARG_H
#define ARG_H

#define ARGSIZE 64

typedef struct {
    int charc;
    char charv[ARGSIZE + 1];
} arg_t;

void arg_init(arg_t *parg);

#endif // ARG_H
