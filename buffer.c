#include "buffer.h"

void buffer_init(buffer_t *pbuffer) {
    pbuffer->charc = 0;
    pbuffer->charv[pbuffer->charc] = '\0';
}

void buffer_read(buffer_t *pbuffer) {
    buffer_init(pbuffer);
    if (scanf("%" XSTR(BUFFERSIZE) "[^\n]", pbuffer->charv) < 0) exit(0);
    pbuffer->charc = strlen(pbuffer->charv);
    scanf("%*[^\n]");
    scanf("%*c");
}

int buffer_scanf(buffer_t *pbuffer, int offset, arg_t *parg) {
    if (pbuffer->charc < offset || offset < 0) return -1;
    char *cursor = pbuffer->charv + offset;
    while (isspace(*cursor)) cursor++;
    arg_init(parg);
    sscanf(cursor, "%" XSTR(ARGSIZE) "s", parg->charv);
    cursor += (parg->charc = strlen(parg->charv));
    while (!isspace(*cursor) && *cursor) cursor++;
    return cursor - pbuffer->charv;
}
