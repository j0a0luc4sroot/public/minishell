#include "utils.h"

pid_t efork(void) {
    pid_t pid;
    if ((pid = fork()) >= 0) return pid;
    fprintf(stderr, "Fork Error: %s\n", strerror(errno));
    exit(-1);
}

void epipe(int pipefd[2]) {
    if (!pipe(pipefd)) return;
    fprintf(stderr, "Pipe Error: %s\n", strerror(errno));
    exit(-1);
}
