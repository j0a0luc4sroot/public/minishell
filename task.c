#include "task.h"

void task_init(task_t *ptask) {
    ptask->infd = STDIN_FILENO;
    ptask->outfd = STDOUT_FILENO;
    task_argvclear(ptask);
}

void task_setinfd(task_t *ptask, int infd) {
    if (ptask->infd != STDIN_FILENO)
        close(ptask->infd);
    ptask->infd = infd;
}

void task_setoutfd(task_t *ptask, int outfd) {
    if (ptask->outfd != STDOUT_FILENO)
        close(ptask->outfd);
    ptask->outfd = outfd;
}

int task_argvpush(task_t *ptask, arg_t *arg) {
    if (ptask->argc == ARGVSIZE) return -1;
    strcpy(ptask->heap[ptask->argc].charv, arg->charv);
    ptask->heap[ptask->argc].charc = arg->charc;
    ptask->argv[ptask->argc] = ptask->heap[ptask->argc].charv;
    ptask->argv[++ptask->argc] = NULL;
    return 0;
}

void task_argvclear(task_t *ptask) {
    ptask->argc = -1;
    ptask->argv[++ptask->argc] = NULL;
}
