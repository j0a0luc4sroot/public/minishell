#ifndef BUFFER_H
#define BUFFER_H

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "arg.h"

#define BUFFERSIZE 1024

typedef struct {
    int charc;
    char charv[BUFFERSIZE + 1];
} buffer_t;

void buffer_init(buffer_t *pbuffer);
void buffer_read(buffer_t *pbuffer);
int buffer_scanf(buffer_t *pbuffer, int offset, arg_t *parg);

#endif // BUFFER_H
