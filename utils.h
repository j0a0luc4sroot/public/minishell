#ifndef UTILS_H
#define UTILS_H

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define STR(X) #X
#define XSTR(X) STR(X)

pid_t efork(void);
void epipe(int [2]);

#endif // UTILS_H
