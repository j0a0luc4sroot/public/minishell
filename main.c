#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#include "arg.h"
#include "buffer.h"
#include "utils.h"
#include "task.h"

int running = 1;

task_t task;
buffer_t buffer;

void init(void);
int parsetask(void);
void exectask(void);
void cleartask(void);

enum {
    PENOIN,
    PENOEXIN,
    PENORDIN,
    PENOOUT,
    PENOWROUT
} perrnos;

char *pstrerror[] = {
    [PENOIN] = "no input file specified",
    [PENOEXIN] = "input file doesn't exist",
    [PENORDIN] = "input file can't be read",
    [PENOOUT] = "no output file specified",
    [PENOWROUT] = "output file can't be overwritten"
};

int perrno;

void init(void) {
    task_init(&task);
    buffer_init(&buffer);
}

int parsetask(void) {
    arg_t arg;

    int offset = 0;
    while (offset < buffer.charc) {
        offset = buffer_scanf(&buffer, offset, &arg);
        if (!strcmp(arg.charv, "<")) {
            if (!buffer.charv[offset]) {
                perrno = PENOIN;
                return -1;
            }
            offset = buffer_scanf(&buffer, offset, &arg);
            if (access(arg.charv, F_OK) < 0) {
                perrno = PENOEXIN;
                return -1;
            } else if (access(arg.charv, R_OK) < 0) {
                perrno = PENORDIN;
                return -1;
            }
            int infd = open(arg.charv, O_RDONLY);
            task_setinfd(&task, infd);
        } else if (!strcmp(arg.charv, ">")) {
            if (!buffer.charv[offset]) {
                perrno = PENOOUT;
                return -1;
            }
            offset = buffer_scanf(&buffer, offset, &arg);
            if (access(arg.charv, F_OK) == 0 && access(arg.charv, W_OK) < 0) {
                perrno = PENOWROUT;
                return -1;
            }
            int outfd = open(arg.charv, O_CREAT | O_WRONLY | O_TRUNC, 0644);
            task_setoutfd(&task, outfd);
        } else if (!strcmp(arg.charv, "|")) {
            int pipefd[2];
            epipe(pipefd);
            pid_t pid = efork();
            if (!pid) {
                close(pipefd[STDIN_FILENO]);
                task_setoutfd(&task, pipefd[STDOUT_FILENO]);
                exectask();
            }
            task_setinfd(&task, pipefd[STDIN_FILENO]);
            close(pipefd[STDOUT_FILENO]);
            task_argvclear(&task);
        } else
            task_argvpush(&task, &arg);
    }
    return 0;
}

void exectask(void) {
    dup2(task.infd, STDIN_FILENO);
    dup2(task.outfd, STDOUT_FILENO);
    execve(task.argv[0], task.argv, NULL);
    fprintf(stderr, "Exec Error: %s: %s\n", task.argv[0], strerror(errno));
    exit(-1);
}

void cleartask(void) {
    task_setinfd(&task, STDIN_FILENO);
    task_setoutfd(&task, STDOUT_FILENO);
    task_argvclear(&task);
}

int main() {
    init();
    while (running) {
        printf("cmd> ");
        buffer_read(&buffer);
        if (parsetask() < 0)
            fprintf(stderr, "Parse Error: %s\n", pstrerror[perrno]);
        else {
            pid_t pid = efork();
            if (!pid)
                exectask();
            waitpid(pid, NULL, 0);
        }
        cleartask();
    }
}
